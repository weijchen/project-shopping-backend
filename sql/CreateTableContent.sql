CREATE DATABASE IF NOT EXISTS shopping_content;
USE shopping_content;

-- Create Products Table
DROP TABLE IF EXISTS Products;
CREATE TABLE IF NOT EXISTS Products (
	product_id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100),
    description VARCHAR(200),
    wholesale_price INT,
    retail_price INT,
    quantity INT
);
INSERT INTO Products (title, description, wholesale_price, retail_price, quantity) Values 
('apple', 'good for health', 3, 5, 1000),
('banana', 'yummy yellow fruit', 1, 3, 1000),
('lego', 'play with your children', 10, 30, 1000),
('laptop', 'cheapest laptop in the world', 150, 300, 1000),
('beef shank', 'best choice for your dinner', 20, 50, 1000);
SELECT * FROM Products;


-- Create Orders Table
DROP TABLE IF EXISTS Orders;
CREATE TABLE IF NOT EXISTS Orders (
	order_id INT AUTO_INCREMENT PRIMARY KEY,
    placement_time TIMESTAMP,
    status int,
    user_id INT
);
INSERT INTO Orders (placement_time, status, user_id) Values 
('2023-06-09 10:00:00', 0, 2),
('2023-06-09 11:00:00', 0, 2),
('2023-06-09 12:00:00', 1, 2),
('2023-06-09 13:00:00', 1, 2),
('2023-06-09 14:00:00', 1, 2),
('2023-06-09 15:00:00', 1, 2),
('2023-06-09 16:00:00', 1, 2),
('2023-06-09 17:00:00', 0, 2),
('2023-06-09 18:00:00', 2, 2),
('2023-06-09 19:00:00', 2, 2),
('2023-06-09 20:00:00', 2, 3);
SELECT * FROM Orders;


-- Create OrderItems Table
DROP TABLE IF EXISTS OrderItems;
CREATE TABLE IF NOT EXISTS OrderItems (
	orderitem_id INT AUTO_INCREMENT PRIMARY KEY,
	purchase_price INT,
    wholesale_price INT,
    quantity INT,
    order_id INT,
    FOREIGN KEY (order_id) REFERENCES Orders(order_id),
    product_id INT,
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);
INSERT INTO OrderItems (purchase_price, wholesale_price, quantity, order_id, product_id) Values 
(5, 3, 1, 1, 1),
(3, 1, 1, 1, 2),
(30, 10, 1, 1, 3),
(5, 3, 1, 2, 1),
(300, 150, 1, 2, 4),
(50, 20, 1, 2, 5),
(3, 1, 1, 3, 2),
(30, 10, 1, 3, 3),
(50, 20, 1, 3, 5),
(30, 10, 1, 4, 3),
(300, 150, 1, 4, 4),
(50, 20, 1, 4, 5),
(3, 1, 1, 5, 2),
(300, 150, 1, 5, 4),
(50, 20, 1, 5, 5),
(5, 3, 1, 6, 1),
(300, 150, 1, 6, 4),
(3, 1, 1000000, 7, 2),
(50, 20, 1, 7, 5),
(3, 1, 1, 8, 2),
(30, 10, 1, 8, 3),
(300, 150, 1, 8, 4),
(50, 20, 1, 8, 5);
SELECT * FROM OrderItems;


-- Create Watchlists Table
DROP TABLE IF EXISTS WatchLists;
CREATE TABLE IF NOT EXISTS WatchLists (
	list_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNIQUE
);
INSERT INTO WatchLists (user_id) Values 
(2);
SELECT * FROM WatchLists;


-- Create WatchListProduct junction
DROP TABLE IF EXISTS WatchItems;
CREATE TABLE IF NOT EXISTS WatchItems (
	list_id int,
    product_id int,
    FOREIGN KEY (list_id) REFERENCES WatchLists(list_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);
INSERT INTO WatchItems (list_id, product_id) Values 
(1, 2),
(1, 3); 
SELECT * FROM WatchItems;