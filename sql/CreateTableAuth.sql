CREATE DATABASE IF NOT EXISTS shopping_auth;
USE shopping_auth;

-- Create User Table
DROP TABLE IF EXISTS Users;
CREATE TABLE IF NOT EXISTS Users (
	user_id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(42) UNIQUE,
    username VARCHAR(42) UNIQUE,
    password VARCHAR(42),
    is_admin BOOL
);
INSERT INTO Users (email, username, password, is_admin) Values 
("jimmy@gmail.com", "jimmy", "123", true),
("test@gmail.com", "test", "123", false);
SELECT * FROM Users;