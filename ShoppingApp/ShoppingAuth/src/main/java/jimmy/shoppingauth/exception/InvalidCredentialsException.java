package jimmy.shoppingauth.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class InvalidCredentialsException extends UsernameNotFoundException {
    public InvalidCredentialsException() {
        super("Incorrect credentials, please try again");
    }
}
