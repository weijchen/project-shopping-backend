package jimmy.shoppingauth.service;

import jimmy.shoppingauth.dao.UserDao;
import jimmy.shoppingauth.domain.entity.User;
import jimmy.shoppingauth.exception.InvalidCredentialsException;
import jimmy.shoppingauth.security.AuthUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    public void addUser(String email, String username, String password) throws DataIntegrityViolationException {
        User user = new User(email, username, password);
        userDao.addUser(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws InvalidCredentialsException {
        Optional<User> userOptional = userDao.loadUserByUsername(username);

        if (!userOptional.isPresent()) {
            throw new InvalidCredentialsException();
        }

        User user = userOptional.get();

        return AuthUserDetail.builder()
                .username(user.getUsername())
                .password(new BCryptPasswordEncoder().encode(user.getPassword()))
                .userId(user.getId())
                .authorities(getAuthoritiesFromUser(user))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();
    }

    private List<GrantedAuthority> getAuthoritiesFromUser(User user) {
        List<GrantedAuthority> userAuthorities = new ArrayList<>();

        if (user.isAdmin()) {
            userAuthorities.add(new SimpleGrantedAuthority("admin"));
        } else {
            userAuthorities.add(new SimpleGrantedAuthority("normal"));
        }

        return userAuthorities;
    }
}
