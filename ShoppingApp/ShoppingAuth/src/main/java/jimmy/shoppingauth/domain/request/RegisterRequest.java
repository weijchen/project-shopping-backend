package jimmy.shoppingauth.domain.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class RegisterRequest {

    @NotNull(message = "Email is required")
    private String email;

    @NotNull(message = "Username is required")
    private String username;

    @NotNull(message = "Password is required")
    private String password;
}
