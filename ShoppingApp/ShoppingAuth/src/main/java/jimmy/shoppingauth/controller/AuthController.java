package jimmy.shoppingauth.controller;

import jimmy.shoppingauth.domain.request.LoginRequest;
import jimmy.shoppingauth.domain.request.RegisterRequest;
import jimmy.shoppingauth.domain.response.AuthResponse;
import jimmy.shoppingauth.security.AuthUserDetail;
import jimmy.shoppingauth.security.JwtProvider;
import jimmy.shoppingauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AuthController {
    private AuthenticationManager authenticationManager;
    private JwtProvider jwtProvider;
    private UserService userService;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, JwtProvider jwtProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.userService = userService;
    }

    @PostMapping("auth/login")
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest request) {

        Authentication authentication;

        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
            );
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Incorrect credentials, please try again.");
        }

        AuthUserDetail authUserDetail = (AuthUserDetail) authentication.getPrincipal();

        String token = jwtProvider.createToken(authUserDetail);

        return new ResponseEntity<>(
                AuthResponse.builder()
                        .message("Welcome " + authUserDetail.getUsername())
                        .token(token)
                        .build(), HttpStatus.OK);
    }

    @PostMapping("auth/register")
    public ResponseEntity<AuthResponse> register(@RequestBody RegisterRequest request,
                                                 BindingResult bindingResult) throws DataIntegrityViolationException {
        if (bindingResult.hasErrors()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        userService.addUser(request.getEmail(), request.getUsername(), request.getPassword());

        return new ResponseEntity<>(
                AuthResponse.builder()
                        .message("Registered, please log in with your new account")
                        .build(), HttpStatus.OK);
    }
}
