package jimmy.shoppingcontent.controller;

import jimmy.shoppingcontent.domain.dto.response.GenericResponse;
import jimmy.shoppingcontent.domain.dto.response.MostFreqProduct;
import jimmy.shoppingcontent.domain.dto.response.MostProfitProduct;
import jimmy.shoppingcontent.domain.dto.response.MostRecentProduct;
import jimmy.shoppingcontent.domain.dto.request.ProductRequest;
import jimmy.shoppingcontent.domain.entity.Product;
import jimmy.shoppingcontent.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("products")
public class ProductController {
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ResponseEntity<List<Product>> getAllProducts(Authentication authentication) throws EmptyResultDataAccessException {
        Object[] auth = authentication.getAuthorities().toArray();
        boolean isAdmin = auth[0].toString().equals("admin");
        List<Product> products = productService.getAllProducts(isAdmin);
        if (products.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") int id,
                                                  Authentication authentication) throws EmptyResultDataAccessException {
        Object[] auth = authentication.getAuthorities().toArray();
        boolean isAdmin = auth[0].toString().equals("admin");
        Product product = productService.getProductById(id, isAdmin);
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<GenericResponse> addProduct(@Valid @RequestBody ProductRequest product,
                                                      BindingResult bindingResult) throws DataIntegrityViolationException {
        if (bindingResult.hasErrors()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Product newProduct = Product.builder()
                .title(product.getTitle())
                .description(product.getDescription())
                .wholesalePrice(product.getWholesalePrice())
                .retailPrice(product.getRetailPrice())
                .quantity(product.getQuantity())
                .build();

        productService.addProduct(newProduct);

        return new ResponseEntity<>(GenericResponse.builder()
                .message("Product created")
                .build(), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<GenericResponse> updateProduct(@PathVariable("id") int id,
                                                         @Valid @RequestBody ProductRequest product,
                                                         BindingResult bindingResult
    ) throws DataIntegrityViolationException {
        if (bindingResult.hasErrors()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Product newProduct = Product.builder()
                .title(product.getTitle())
                .description(product.getDescription())
                .wholesalePrice(product.getWholesalePrice())
                .retailPrice(product.getRetailPrice())
                .quantity(product.getQuantity())
                .build();

        productService.updateProduct(id, newProduct);

        return new ResponseEntity<>(GenericResponse.builder()
                .message("Product updated")
                .build(), HttpStatus.CREATED);
    }

    @GetMapping("/frequent/{number}")
    public ResponseEntity<List<MostFreqProduct>> getMostFrequentlyPurchasedNProduct(@PathVariable("number") int number,
                                                                                    Authentication authentication) {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        List<MostFreqProduct> products = productService.getMostFrequentlyPurchasedNProduct(userId, isAdmin, number);
        if (products.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/recent/{number}")
    public ResponseEntity<List<MostRecentProduct>> getMostRecentPurchasedNProduct(@PathVariable("number") int number,
                                                                                  Authentication authentication) {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        List<MostRecentProduct> products = productService.getMostRecentPurchasedNProduct(userId, isAdmin, number);
        if (products.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/profit/{number}")
    public ResponseEntity<List<MostProfitProduct>> getMostProfitablePurchasedNProduct(@PathVariable("number") int number,
                                                                                      Authentication authentication) {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        List<MostProfitProduct> products = productService.getMostProfitableNProduct(userId, isAdmin, number);
        if (products.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/popular/{number}")
    public ResponseEntity<List<MostFreqProduct>> getMostPopularPurchasedNProduct(@PathVariable("number") int number,
                                                                                 Authentication authentication) {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        List<MostFreqProduct> products = productService.getMostPopularPurchasedNProduct(userId, isAdmin, number);
        if (products.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
