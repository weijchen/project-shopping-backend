package jimmy.shoppingcontent.controller;

import jimmy.shoppingcontent.domain.dto.request.NewOrderItemRequest;
import jimmy.shoppingcontent.domain.dto.request.NewOrderRequest;
import jimmy.shoppingcontent.domain.dto.response.GenericResponse;
import jimmy.shoppingcontent.domain.entity.Order;
import jimmy.shoppingcontent.domain.entity.OrderItem;
import jimmy.shoppingcontent.domain.entity.Product;
import jimmy.shoppingcontent.domain.enumClass.OrderStatus;
import jimmy.shoppingcontent.exception.InvalidOrderStatusUpdateException;
import jimmy.shoppingcontent.exception.NotEnoughInventoryException;
import jimmy.shoppingcontent.service.OrderService;
import jimmy.shoppingcontent.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("orders")
public class OrderController {
    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    private ProductService productService;

    @Autowired
    public OrderController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ResponseEntity<List<Order>> getAllOrders(Authentication authentication) throws EmptyResultDataAccessException {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");

        List<Order> orders = orderService.getAllOrders(userId, isAdmin);
        if (orders.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable int id,
                                              Authentication authentication) throws EmptyResultDataAccessException {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");

        Order order = orderService.getOrderById(userId, id, isAdmin);

        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PatchMapping("/{id}/cancel")
    public ResponseEntity<GenericResponse> cancelOrderStatus(@PathVariable("id") int order_id,
                                                             Authentication authentication) throws EmptyResultDataAccessException, DataIntegrityViolationException, InvalidOrderStatusUpdateException {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        orderService.updateOrderStatus(userId, order_id, isAdmin, OrderStatus.CANCELED.ordinal());

        Order orderToRevert = orderService.getOrderById(userId, order_id, true);

        for (OrderItem item : orderToRevert.getOrderItemList()) {
            Product product = productService.getProductById(item.getProduct_id(), true);
            product.setQuantity(product.getQuantity() + item.getQuantity());
            productService.updateProduct(item.getProduct_id(), product);
        }

        return new ResponseEntity<>(GenericResponse.builder()
                .message("Order canceled")
                .build(), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}/complete")
    public ResponseEntity<GenericResponse> completeOrderStatus(@PathVariable("id") int id,
                                                               Authentication authentication
    ) throws EmptyResultDataAccessException, DataIntegrityViolationException, InvalidOrderStatusUpdateException {
        Object[] auth = authentication.getAuthorities().toArray();
        int userId = (int) authentication.getCredentials();
        boolean isAdmin = auth[0].toString().equals("admin");
        orderService.updateOrderStatus(userId, id, isAdmin, OrderStatus.COMPLETED.ordinal());
        return new ResponseEntity<>(GenericResponse.builder()
                .message("Order completed")
                .build(), HttpStatus.CREATED);
    }



    @PostMapping()
    public ResponseEntity<GenericResponse> createOrder(@Valid @RequestBody NewOrderRequest orderRequest,
                                                       Authentication authentication,
                                                       BindingResult bindingResult) throws EmptyResultDataAccessException, NotEnoughInventoryException {
        if (bindingResult.hasErrors()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        int userId = (int) authentication.getCredentials();

        List<NewOrderItemRequest> orderItemRequests = orderRequest.getNewOrderItemRequestList();

        // check all product has enough stock
        for (NewOrderItemRequest itemRequest : orderItemRequests) {
            if (!productService.checkProductAmount(itemRequest.getProduct_id(), itemRequest.getQuantity())) {
                throw new NotEnoughInventoryException("Product: " + productService.getProductById(itemRequest.getProduct_id(), false).getTitle() + " has no enough amount");
            }
        }

        // create Order
        List<OrderItem> orderItemList = new ArrayList<>();
        Order order = Order.builder()
                .user_id(userId)
                .status(OrderStatus.PROCESSING.ordinal())
                .placementTime(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        // update product amount in DB and link OrderItem with Order
        for (NewOrderItemRequest itemRequest : orderItemRequests) {
            Product product = productService.getProductById(itemRequest.getProduct_id(), true);
            product.setQuantity(product.getQuantity() - itemRequest.getQuantity());
            productService.updateProduct(itemRequest.getProduct_id(), product);
            orderItemList.add(
                    OrderItem.builder()
                            .order(order)
                            .product_id(itemRequest.getProduct_id())
                            .quantity(itemRequest.getQuantity())
                            .purchasePrice(product.getRetailPrice())
                            .wholesaleAtPurchase(product.getWholesalePrice())
                            .build());
        }

        // update Order in DB
        order.setOrderItemList(orderItemList);
        orderService.addNewOrder(order);

        return new ResponseEntity<>(GenericResponse.builder()
                .message("Order created")
                .build(), HttpStatus.CREATED);
    }
}
