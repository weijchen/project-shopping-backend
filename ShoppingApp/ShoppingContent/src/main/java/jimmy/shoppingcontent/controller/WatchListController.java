package jimmy.shoppingcontent.controller;

import jimmy.shoppingcontent.domain.dto.response.GenericResponse;
import jimmy.shoppingcontent.domain.entity.WatchList;
import jimmy.shoppingcontent.service.WatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("watchlist")
public class WatchListController {
    private WatchListService watchListService;

    @Autowired
    public void setWatchListService(WatchListService watchListService) {
        this.watchListService = watchListService;
    }

    @GetMapping()
    public ResponseEntity<WatchList> getWatchListByUserId(Authentication authentication) {
        int userId = (int) authentication.getCredentials();
        WatchList watchList = watchListService.getWatchListByUserId(userId);
        return new ResponseEntity<>(watchList, HttpStatus.OK);
    }

    @PostMapping("/product/{id}")
    public ResponseEntity<GenericResponse> addToWatchList(@PathVariable("id") int product_id,
                                                          Authentication authentication) throws DataIntegrityViolationException {
        int userId = (int) authentication.getCredentials();
        if (watchListService.isInWatchList(userId, product_id)) {
            return new ResponseEntity<>(GenericResponse.builder().message("Product already in watch list").build(), HttpStatus.BAD_REQUEST);
        }
        watchListService.addToWatchList(userId, product_id);
        return new ResponseEntity<>(GenericResponse.builder().message("Product added to watch list").build(), HttpStatus.OK);
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<WatchList> removeFromWatchList(@PathVariable("id") int product_id,
                                                         Authentication authentication) throws DataIntegrityViolationException {
        int userId = (int) authentication.getCredentials();
        if (!watchListService.isInWatchList(userId, product_id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        watchListService.deleteFromWatchList(userId, product_id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
