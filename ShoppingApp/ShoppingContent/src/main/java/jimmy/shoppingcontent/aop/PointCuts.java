package jimmy.shoppingcontent.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class PointCuts {
    @Pointcut("within(jimmy.shoppingcontent.controller.*)")
    public void inControllerLayer(){}

    @Pointcut("execution(* jimmy.shoppingcontent.dao.*Dao.get*())")
    public void inDAOLayer(){}
}
