package jimmy.shoppingcontent.aop;

import jimmy.shoppingcontent.domain.dto.response.ErrorResponse;
import jimmy.shoppingcontent.exception.NotEnoughInventoryException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AOPExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        return new ResponseEntity<>(ErrorResponse.builder().message("Exception: " + e.getMessage()).build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolationException(Exception e) {
        System.out.println("here");
        return new ResponseEntity<>(ErrorResponse.builder().message("DataIntegrityViolationException: " + e.getMessage()).build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    public ResponseEntity<ErrorResponse> handleEmptyResultDataAccessException(Exception e) {
        return new ResponseEntity<>(ErrorResponse.builder().message("EmptyResultDataAccessException: " + e.getMessage()).build(), HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(value = {NotEnoughInventoryException.class})
    public ResponseEntity<ErrorResponse> handleNotEnoughInventoryException(Exception e) {
        return new ResponseEntity<>(ErrorResponse.builder().message("NotEnoughInventoryException: " + e.getMessage()).build(), HttpStatus.OK);
    }
}
