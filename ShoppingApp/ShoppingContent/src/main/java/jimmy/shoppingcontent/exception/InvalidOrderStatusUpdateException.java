package jimmy.shoppingcontent.exception;

public class InvalidOrderStatusUpdateException extends Exception{
    public InvalidOrderStatusUpdateException(String s) {
        super(s);
    }
}
