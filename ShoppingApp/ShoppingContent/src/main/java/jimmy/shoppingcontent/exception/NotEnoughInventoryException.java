package jimmy.shoppingcontent.exception;

public class NotEnoughInventoryException extends Exception{
    public NotEnoughInventoryException(String s) {
        super(s);
    }
}
