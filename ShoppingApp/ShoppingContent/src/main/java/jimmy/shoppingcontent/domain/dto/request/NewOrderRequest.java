package jimmy.shoppingcontent.domain.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewOrderRequest {
    @NotNull(message = "OrderItemList is required")
    List<NewOrderItemRequest> newOrderItemRequestList;
}
