package jimmy.shoppingcontent.domain.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewOrderItemRequest {
    @NotNull(message = "Product ID is required")
    private int product_id;

    @NotNull(message = "Quantity is required")
    private int quantity;
}
