package jimmy.shoppingcontent.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "OrderItems")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderitem_id", unique = true, nullable = false)
    private int id;

    @Column(name = "purchase_price", nullable = false)
    private int purchasePrice;

    @Column(name = "wholesale_price", nullable = false)
    private int wholesaleAtPurchase;

    @Column(nullable = false)
    private int quantity;

    @Column(nullable = false)
    private int product_id;

//    @Column(nullable = false)
//    private int order_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    @JsonBackReference
    private Order order;

    @Transient
    private Product product;

    public void setProduct(Product product) {
        this.product = product;
    }

    public OrderItem(int purchasePrice, int wholesaleAtPurchase, int quantity, int product_id) {
        this.purchasePrice = purchasePrice;
        this.wholesaleAtPurchase = wholesaleAtPurchase;
        this.quantity = quantity;
        this.product_id = product_id;
    }
}
