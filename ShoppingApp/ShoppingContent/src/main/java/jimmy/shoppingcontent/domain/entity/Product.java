package jimmy.shoppingcontent.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Products")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", unique = true, nullable = false)
    private int id;

    @Column(nullable = false)
    private String title;

    @Column
    private String description;

    @Column(name = "wholesale_price")
    private int wholesalePrice;

    @Column(name = "retail_price", nullable = false)
    private int retailPrice;

    @Column(nullable = false)
    private int quantity;

    @ManyToMany(mappedBy = "watchItemList")
    @JsonBackReference
    private Set<WatchList> watchLists = new HashSet<>();

    public Product(int id, String title, String description, int retailPrice) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.retailPrice = retailPrice;
    }
}
