package jimmy.shoppingcontent.domain.dto.response;

import jimmy.shoppingcontent.domain.entity.Product;
import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MostProfitProduct {
    private Product product;
    private int profit;
}
