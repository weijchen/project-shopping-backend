package jimmy.shoppingcontent.domain.dto.response;

import jimmy.shoppingcontent.domain.entity.Product;
import lombok.*;

import java.sql.Timestamp;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MostRecentProduct {
    private Product product;
    private Timestamp timestamp;
}
