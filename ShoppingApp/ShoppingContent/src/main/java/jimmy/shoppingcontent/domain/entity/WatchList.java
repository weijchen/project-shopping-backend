package jimmy.shoppingcontent.domain.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "WatchLists")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WatchList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "list_id", unique = true, nullable = false)
    private int id;

    @Column(unique = true)
    private int user_id;

    @ManyToMany
    @JoinTable(name = "WatchItems",
            joinColumns = {@JoinColumn(name = "list_id")}, inverseJoinColumns = {@JoinColumn(name = "product_id")})
    @JsonManagedReference
    private Set<Product> watchItemList = new HashSet<>();

    public WatchList(int user_id) {
        this.user_id = user_id;
    }

    public void addWatchItem(Product product) {
        watchItemList.add(product);
    }

    public void removeWatchItem(Product product) {
        watchItemList.remove(product);
    }
}
