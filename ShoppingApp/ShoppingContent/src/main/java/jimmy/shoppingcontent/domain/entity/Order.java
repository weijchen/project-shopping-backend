package jimmy.shoppingcontent.domain.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jimmy.shoppingcontent.domain.enumClass.OrderStatus;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Orders")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", unique = true, nullable = false)
    private int id;

    @Column(name = "placement_time", nullable = false)
    private Timestamp placementTime;

    @Column(nullable = false)
    @Getter(AccessLevel.NONE)
    private int status;

    @Column(nullable = false)
    private int user_id;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<OrderItem> orderItemList = new ArrayList<>();

    public String getStatus() {
        return OrderStatus.getEnumByOrdinal(status);
    }
}
