package jimmy.shoppingcontent.domain.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusRequest {
    @NotNull(message = "Status is required")
    private String status;

}
