package jimmy.shoppingcontent.domain.dto.response;

import jimmy.shoppingcontent.domain.entity.Product;
import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MostFreqProduct {
    private Product product;
    private int count;
}
