package jimmy.shoppingcontent.domain.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    @NotNull(message = "Title is required")
    private String title;

    @NotNull(message = "Description is required")
    private String description;

    @NotNull(message = "Wholesale price is required")
    private int wholesalePrice;

    @NotNull(message = "Retail price is required")
    private int retailPrice;

    @NotNull(message = "Quantity is required")
    private int quantity;
}
