package jimmy.shoppingcontent.domain.enumClass;

public enum OrderStatus {
    PROCESSING("processing"),
    COMPLETED("completed"),
    CANCELED("canceled");

    public final String label;

    private OrderStatus(String label) {
        this.label = label;
    }

    public static Integer getEnumOrdinalByString(String code) {
        for (OrderStatus e : OrderStatus.values()) {
            if (e.label.equals(code)) return e.ordinal();
        }
        return null;
    }

    public static String getEnumByOrdinal(int ordinal) {
        for (OrderStatus e : OrderStatus.values()) {
            if (e.ordinal() == ordinal) return e.label;
        }
        return null;
    }
}
