package jimmy.shoppingcontent.config.security;

import jimmy.shoppingcontent.security.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private JwtFilter jwtFilter;

    @Autowired
    public void setJwtFilter(JwtFilter jwtFilter) {
        this.jwtFilter = jwtFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/products").hasAnyAuthority("admin")
                .antMatchers(HttpMethod.PATCH, "/products/*").hasAnyAuthority("admin")
                .antMatchers("/products/profit/*").hasAnyAuthority("admin")
                .antMatchers("/products/popular/*").hasAnyAuthority("admin")
                .antMatchers("/products/frequent/*").hasAnyAuthority("normal")
                .antMatchers("/products/recent/*").hasAnyAuthority("normal")
                .antMatchers("/products").permitAll()
                .antMatchers(HttpMethod.PATCH, "/orders/*/complete").hasAnyAuthority("admin")
                .antMatchers(HttpMethod.PATCH, "/orders/*/cancel").permitAll()
                .antMatchers("/orders").permitAll()
                .antMatchers("/orders/*").permitAll()
                .antMatchers("/watchlist").hasAnyAuthority("normal")
                .antMatchers("/watchlist/product/*").hasAnyAuthority("normal")
                .anyRequest()
                .authenticated();
    }
}