package jimmy.shoppingcontent.dao;

import jimmy.shoppingcontent.domain.entity.OrderItem;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderItemDao extends AbstractHibernateDao<OrderItem> {
    public OrderItemDao() {
        setClazz(OrderItem.class);
    }

    public OrderItem getOrderItemById(int id) {
        return this.findById(id);
    }

    public List<OrderItem> getAllOrderItems() {
        return this.getAll();
    }

    public void addOrderItem(OrderItem orderItem) {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.save(orderItem);
        tx.commit();
    }

    public void deleteOrderItem(OrderItem orderItem) {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        session.delete(orderItem);
        tx.commit();
    }
}
