package jimmy.shoppingcontent.dao;

import jimmy.shoppingcontent.domain.entity.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProductDao extends AbstractHibernateDao<Product> {

    public ProductDao() {
        setClazz(Product.class);
    }

    public Product getProductById(int id, boolean isAdmin) throws EmptyResultDataAccessException {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Product> cr = cb.createQuery(Product.class);
        Root<Product> root = cr.from(Product.class);
        try {
            if (isAdmin) {
                return this.findById(id);
            } else {
                cr.multiselect(root.get("id"), root.get("title"), root.get("description"), root.get("retailPrice"));
                cr.where(cb.equal(root.get("id"), id));
                Query<Product> query = session.createQuery(cr);
                List<Product> products = query.getResultList();
                return products.size() == 0 ? null : products.get(0);
            }
        } catch (Exception e) {
            throw new EmptyResultDataAccessException(1);
        }
    }

    public List<Product> getAllProducts(boolean isAdmin) throws EmptyResultDataAccessException {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Product> cr = cb.createQuery(Product.class);
        Root<Product> root = cr.from(Product.class);
        try {
            if (isAdmin) {
                return this.getAll();
            } else {
                cr.where(cb.ge(root.get("quantity"), 1));
                cr.multiselect(root.get("id"), root.get("title"), root.get("description"), root.get("retailPrice"));
                Query<Product> query = session.createQuery(cr);
                return query.getResultList();
            }
        } catch (Exception e) {
            throw new EmptyResultDataAccessException(1);
        }
    }

    public void addProduct(Product product) throws DataIntegrityViolationException {
        try {
            Session session = this.getCurrentSession();
            Transaction tx = session.beginTransaction();
            session.save(product);
            tx.commit();
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }

    public void deleteProduct(Product product) throws DataIntegrityViolationException {
        try {
            Session session = this.getCurrentSession();
            Transaction tx = session.beginTransaction();
            session.delete(product);
            tx.commit();
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }

    public void updateProduct(int id, Product product) throws DataIntegrityViolationException {
        try {
            Session session = this.getCurrentSession();
            Transaction tx = session.beginTransaction();
            Product productToChange = session.load(Product.class, id);
            productToChange.setTitle(product.getTitle());
            productToChange.setDescription(product.getDescription());
            productToChange.setWholesalePrice(product.getWholesalePrice());
            productToChange.setRetailPrice(product.getRetailPrice());
            productToChange.setQuantity(product.getQuantity());
            session.update(productToChange);
            tx.commit();
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }
}
