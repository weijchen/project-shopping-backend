package jimmy.shoppingcontent.dao;

import jimmy.shoppingcontent.domain.entity.Product;
import jimmy.shoppingcontent.domain.entity.WatchList;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;

@Repository
public class WatchListDao extends AbstractHibernateDao<WatchList> {
    public WatchListDao() {
        setClazz(WatchList.class);
    }

    public WatchList getWatchListByUserId(int user_id) {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<WatchList> cr = cb.createQuery(WatchList.class);
        Root<WatchList> root = cr.from(WatchList.class);
        root.join("watchItemList", JoinType.LEFT);
        cr.select(root);
        cr.where(cb.equal(root.get("user_id"), user_id));
        Query<WatchList> query = session.createQuery(cr);
        List<WatchList> watchLists = query.getResultList();
        return watchLists.size() != 0 ? watchLists.get(0) : null;
    }

    private void createWatchList(Session session, WatchList watchList) throws DataIntegrityViolationException {
        try {
            session.save(watchList);
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }

    public void addToWatchList(int user_id, Product product) throws DataIntegrityViolationException {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        WatchList watchList = getWatchListByUserId(user_id);
        // create new watchlist if this user never use it
        if (watchList == null) {
            watchList = new WatchList(user_id);
            createWatchList(session, watchList);
        }
        try {
            watchList.addWatchItem(product);
            session.save(watchList);
            tx.commit();
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }

    public boolean isInWatchList(int user_id, int product_id) {
        WatchList watchList = getWatchListByUserId(user_id);
        if (watchList == null) return false;

        Set<Product> productSet = watchList.getWatchItemList();
        for (Product p : productSet) {
            if (p.getId() == product_id) {
                return true;
            }
        }
        return false;
    }

    public void deleteFromWatchList(int user_id, int product_id) throws DataIntegrityViolationException {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        WatchList watchList = getWatchListByUserId(user_id);
        Set<Product> productSet = watchList.getWatchItemList();
        try {
            productSet.removeIf(p -> p.getId() == product_id);
            session.save(watchList);
            tx.commit();
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        }
    }
}
