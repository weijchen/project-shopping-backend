package jimmy.shoppingcontent.dao;

import jimmy.shoppingcontent.domain.entity.Order;
import jimmy.shoppingcontent.domain.enumClass.OrderStatus;
import jimmy.shoppingcontent.exception.InvalidOrderStatusUpdateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Objects;

@Repository
public class OrderDao extends AbstractHibernateDao<Order> {
    public OrderDao() {
        setClazz(Order.class);
    }

    public Order getOrderById(int user_id, int order_id, boolean isAdmin) throws EmptyResultDataAccessException {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cr = cb.createQuery(Order.class);
        Root<Order> root = cr.from(Order.class);
        Query<Order> query;
        try {
            if (isAdmin) {
                root.join("orderItemList", JoinType.LEFT);
                cr.select(root);
                cr.where(cb.equal(root.get("id"), order_id));
            } else {
                cr.select(root);
                Predicate predicate1 = cb.equal(root.get("user_id"), user_id);
                Predicate predicate2 = cb.equal(root.get("id"), order_id);
                cr.where(cb.and(predicate1, predicate2));
            }
            query = session.createQuery(cr);
            List<Order> orders = query.getResultList();
            return orders.get(0);
        } catch (Exception e) {
            throw new EmptyResultDataAccessException(1);
        }
    }

    public List<Order> getAllOrders(int user_id, boolean isAdmin) throws EmptyResultDataAccessException {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cr = cb.createQuery(Order.class);
        Root<Order> root = cr.from(Order.class);
        Query<Order> query;
        try {
            if (isAdmin) {
                cr.select(root);
            } else {
                cr.select(root);
                cr.where(cb.equal(root.get("user_id"), user_id));
            }
            query = session.createQuery(cr);
            return query.getResultList();
        } catch (Exception e) {
            throw new EmptyResultDataAccessException(1);
        }
    }

    public List<Order> getAllCriteriaOrders(int user_id, boolean isAdmin) {
        Session session = this.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cr = cb.createQuery(Order.class);
        Root<Order> root = cr.from(Order.class);
        Query<Order> query;
        if (isAdmin) {
            cr.select(root);
            Predicate predicate1 = cb.equal(root.get("status"), OrderStatus.COMPLETED.ordinal());
            cr.where(predicate1);
        } else {
            cr.select(root);
            Predicate predicate1 = cb.equal(root.get("user_id"), user_id);
            Predicate predicate2 = cb.equal(root.get("status"), OrderStatus.COMPLETED.ordinal());
            Predicate predicate3 = cb.equal(root.get("status"), OrderStatus.PROCESSING.ordinal());
            cr.where(cb.and(predicate1, cb.or(predicate2, predicate3)));
        }
        query = session.createQuery(cr);
        return query.getResultList();

    }

    public void updateOrderStatus(int user_id, int order_id, boolean isAdmin, int status) throws EmptyResultDataAccessException, DataIntegrityViolationException, InvalidOrderStatusUpdateException {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cr = cb.createQuery(Order.class);
        Root<Order> root = cr.from(Order.class);
        Query<Order> query;
        try {
            cr.select(root);
            if (isAdmin) {
                cr.where(cb.equal(root.get("id"), order_id));
            } else {
                Predicate predicate1 = cb.equal(root.get("user_id"), user_id);
                Predicate predicate2 = cb.equal(root.get("id"), order_id);
                cr.where(cb.and(predicate1, predicate2));
            }
            query = session.createQuery(cr);
            try {
                List<Order> orders = query.getResultList();
                Order orderToUpdate = session.load(Order.class, orders.get(0).getId());
                // No need to update an order to its current order status
                if (status == OrderStatus.getEnumOrdinalByString(orderToUpdate.getStatus())) {
                    throw new InvalidOrderStatusUpdateException("status remain");
                }

                 // COMPLETED order cannot be CANCELED
                if (status == OrderStatus.CANCELED.ordinal() && Objects.equals(orderToUpdate.getStatus(), OrderStatus.COMPLETED.label)) {
                    throw new InvalidOrderStatusUpdateException("COMPLETED order cannot be CANCELED");
                }

                // CANCELED order cannot be COMPLETED
                if (status == OrderStatus.COMPLETED.ordinal() && Objects.equals(orderToUpdate.getStatus(), OrderStatus.CANCELED.label)) {
                    throw new InvalidOrderStatusUpdateException("CANCELED order cannot be COMPLETED");
                }
                orderToUpdate.setStatus(status);
                session.update(orderToUpdate);
            } catch (InvalidOrderStatusUpdateException e) {
                throw new InvalidOrderStatusUpdateException(e.getMessage());
            } catch (Exception e) {
                throw new EmptyResultDataAccessException(1);
            }
        } catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException(1);
        } catch (InvalidOrderStatusUpdateException e) {
            throw new InvalidOrderStatusUpdateException(e.getMessage());
        } catch (Exception e) {
            throw new DataIntegrityViolationException(e.getMessage());
        } finally {
            tx.commit();
        }
    }

    public int addOrder(Order order) {
        Session session = this.getCurrentSession();
        Transaction tx = session.beginTransaction();
        int order_id = (int) session.save(order);
        tx.commit();
        return order_id;
    }
}
