package jimmy.shoppingcontent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class ShoppingContentApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingContentApplication.class, args);
    }

}
