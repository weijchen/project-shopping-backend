package jimmy.shoppingcontent.service;

import jimmy.shoppingcontent.dao.ProductDao;
import jimmy.shoppingcontent.dao.WatchListDao;
import jimmy.shoppingcontent.domain.entity.Product;
import jimmy.shoppingcontent.domain.entity.WatchList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class WatchListService {
    private WatchListDao watchListDao;

    @Autowired
    public void setWatchlistDao(WatchListDao watchlistDao) {
        this.watchListDao = watchlistDao;
    }

    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    public WatchList getWatchListByUserId(int user_id) {
        WatchList watchList = this.watchListDao.getWatchListByUserId(user_id);
        WatchList toPresent = new WatchList();
        for (Product product : watchList.getWatchItemList()) {
            if (product.getQuantity() > 0) {
                product.setWholesalePrice(-1);
                product.setQuantity(-1);
                toPresent.addWatchItem(product);
            }
        }
        return toPresent;
    }

    public void addToWatchList(int user_id, int product_id) throws DataIntegrityViolationException {
        Product product = getProductWithId(product_id);
        watchListDao.addToWatchList(user_id, product);
    }

    public boolean isInWatchList(int user_id, int product_id) {
        return watchListDao.isInWatchList(user_id, product_id);
    }

    private Product getProductWithId(int product_id) throws EmptyResultDataAccessException {
        return productDao.getProductById(product_id, false);
    }

    public void deleteFromWatchList(int user_id, int product_id) throws DataIntegrityViolationException {
        watchListDao.deleteFromWatchList(user_id, product_id);
    }
}
