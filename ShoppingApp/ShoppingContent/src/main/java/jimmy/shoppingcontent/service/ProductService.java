package jimmy.shoppingcontent.service;

import jimmy.shoppingcontent.dao.ProductDao;
import jimmy.shoppingcontent.domain.dto.response.MostFreqProduct;
import jimmy.shoppingcontent.domain.dto.response.MostProfitProduct;
import jimmy.shoppingcontent.domain.dto.response.MostRecentProduct;
import jimmy.shoppingcontent.domain.entity.Order;
import jimmy.shoppingcontent.domain.entity.OrderItem;
import jimmy.shoppingcontent.domain.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class ProductService {
    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<Product> getAllProducts(boolean isAdmin) throws EmptyResultDataAccessException {
        List<Product> products = productDao.getAllProducts(isAdmin);
        if (!isAdmin) {
            for (Product product : products) {
                product.setQuantity(-1);
                product.setWholesalePrice(-1);
            }
        }
        return products;
    }

    public Product getProductById(int product_id, boolean isAdmin) throws EmptyResultDataAccessException {
        Product product = productDao.getProductById(product_id, isAdmin);
        if (!isAdmin) {
            product.setQuantity(-1);
            product.setWholesalePrice(-1);
        }
        return product;
    }

    public boolean checkProductAmount(int product_id, int amountToBuy) throws EmptyResultDataAccessException {
        Product product = getProductById(product_id, true);
        return product.getQuantity() >= amountToBuy;
    }

    public void addProduct(Product product) throws DataIntegrityViolationException {
        productDao.addProduct(product);
    }

    public void updateProduct(int id, Product product) throws DataIntegrityViolationException {
        productDao.updateProduct(id, product);
    }

    public List<MostFreqProduct> getMostFrequentlyPurchasedNProduct(int user_id, boolean isAdmin, int number) {
        List<Order> orders = orderService.getAllCriteriaOrders(user_id, isAdmin);
        LinkedList<MostFreqProduct> mostFreqProducts = new LinkedList<>();
        if (orders.size() == 0) return mostFreqProducts;

        // product_id: count
        Map<Integer, Integer> freq = new HashMap<>();
        for (Order o : orders) {
            for (OrderItem item : o.getOrderItemList()) {
                freq.put(item.getProduct_id(), freq.getOrDefault(item.getProduct_id(), 0) + 1);
            }
        }
        PriorityQueue<Map.Entry<Integer, Integer>> output = new PriorityQueue<>((o1, o2) -> {
            if (Objects.equals(o1.getValue(), o2.getValue())) {
                return o2.getKey() - o1.getKey();  // sort id by higher to lower
            }
            return o1.getValue() - o2.getValue();  // sort count by lower to higher
        });

        for (Map.Entry<Integer, Integer> entry : freq.entrySet()) {
            output.add(entry);

            if (output.size() > number) {
                output.poll();
            }
        }

        // reverse insert to output answer with correct ordering
        while (!output.isEmpty()) {
            Product product = productDao.getProductById(output.poll().getKey(), isAdmin);
            mostFreqProducts.addFirst(new MostFreqProduct(product, freq.get(product.getId())));
        }

        return mostFreqProducts;
    }

    public List<MostRecentProduct> getMostRecentPurchasedNProduct(int user_id, boolean isAdmin, int number) {
        List<Order> orders = orderService.getAllCriteriaOrders(user_id, isAdmin);
        LinkedList<MostRecentProduct> mostRecentProducts = new LinkedList<>();
        if (orders.size() == 0) return mostRecentProducts;

        Map<Integer, Timestamp> seenProduct = new HashMap<>();
        PriorityQueue<Product> output = new PriorityQueue<>((o1, o2) -> o1.getId() - o2.getId());

        // iterate orders until having enough amount of products
        for (int i = orders.size() - 1; i >= 0; i--) {
            List<OrderItem> orderItemList = orders.get(i).getOrderItemList();
            for (OrderItem item : orderItemList) {
                if (!seenProduct.containsKey(item.getProduct_id())) {
                    output.add(item.getProduct());
                    seenProduct.put(item.getProduct_id(), orders.get(i).getPlacementTime());
                }
            }

            while (!output.isEmpty()) {
                Product product = output.remove();
                mostRecentProducts.addLast(new MostRecentProduct(product, seenProduct.get(product.getId())));
            }
            if (mostRecentProducts.size() >= number) break;
        }

        // remove redundant
        while (mostRecentProducts.size() > number) {
            mostRecentProducts.removeLast();
        }
        return mostRecentProducts;
    }

    public List<MostProfitProduct> getMostProfitableNProduct(int user_id, boolean isAdmin, int number) {
        List<Order> orders = orderService.getAllCriteriaOrders(user_id, isAdmin);
        LinkedList<MostProfitProduct> mostProfitableProducts = new LinkedList<>();
        if (orders.size() == 0) return mostProfitableProducts;

        // product_id: profit
        Map<Integer, Integer> profits = new HashMap<>();
        for (Order o : orders) {
            for (OrderItem item : o.getOrderItemList()) {
                int profit = (item.getPurchasePrice() - item.getWholesaleAtPurchase()) * item.getQuantity();
                profits.put(item.getProduct_id(), profits.getOrDefault(item.getProduct_id(), 0) + profit);
            }
        }

        PriorityQueue<Map.Entry<Integer, Integer>> output = new PriorityQueue<>((o1, o2) -> {
            if (Objects.equals(o1.getValue(), o2.getValue())) {
                return o2.getKey() - o1.getKey();  // sort id by higher to lower
            }
            return o1.getValue() - o2.getValue();  // sort profit by lower to higher
        });

        for (Map.Entry<Integer, Integer> entry : profits.entrySet()) {
            output.add(entry);

            if (output.size() > number) {
                output.poll();
            }
        }

        // reverse insert to output answer with correct ordering
        while (!output.isEmpty()) {
            Product product = productDao.getProductById(output.poll().getKey(), isAdmin);
            mostProfitableProducts.addFirst(new MostProfitProduct(product, profits.get(product.getId())));
        }

        return mostProfitableProducts;
    }

    public List<MostFreqProduct> getMostPopularPurchasedNProduct(int user_id, boolean isAdmin, int number) {
        List<Order> orders = orderService.getAllCriteriaOrders(user_id, isAdmin);
        LinkedList<MostFreqProduct> mostPopularProducts = new LinkedList<>();
        if (orders.size() == 0) return mostPopularProducts;

        // product_id: total purchased amount
        Map<Integer, Integer> freq = new HashMap<>();
        for (Order o : orders) {
            for (OrderItem item : o.getOrderItemList()) {
                freq.put(item.getProduct_id(), freq.getOrDefault(item.getProduct_id(), 0) + item.getQuantity());
            }
        }
        PriorityQueue<Map.Entry<Integer, Integer>> output = new PriorityQueue<>((o1, o2) -> {
            if (Objects.equals(o1.getValue(), o2.getValue())) {
                return o2.getKey() - o1.getKey();  // sort id by higher to lower
            }
            return o1.getValue() - o2.getValue();  // sort purchased amount by lower to higher
        });

        for (Map.Entry<Integer, Integer> entry : freq.entrySet()) {
            output.add(entry);

            if (output.size() > number) {
                output.poll();
            }
        }

        // reverse insert to output answer with correct ordering
        while (!output.isEmpty()) {
            Product product = productDao.getProductById(output.poll().getKey(), isAdmin);
            mostPopularProducts.addFirst(new MostFreqProduct(product, freq.get(product.getId())));
        }

        return mostPopularProducts;
    }
}
