package jimmy.shoppingcontent.service;

import jimmy.shoppingcontent.dao.OrderDao;
import jimmy.shoppingcontent.dao.OrderItemDao;
import jimmy.shoppingcontent.dao.ProductDao;
import jimmy.shoppingcontent.domain.entity.Order;
import jimmy.shoppingcontent.domain.entity.OrderItem;
import jimmy.shoppingcontent.exception.InvalidOrderStatusUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private OrderDao orderDao;

    @Autowired
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    private OrderItemDao orderItemDao;

    @Autowired
    public void setOrderItemDao(OrderItemDao orderItemDao) {
        this.orderItemDao = orderItemDao;
    }

    public List<Order> getAllOrders(int user_id, boolean isAdmin) throws EmptyResultDataAccessException {
        List<Order> orders = orderDao.getAllOrders(user_id, isAdmin);
        for (Order order : orders) {
            getProductInfo(order.getOrderItemList(), isAdmin);
        }
        return orders;
    }

    public List<Order> getAllCriteriaOrders(int user_id, boolean isAdmin) {
        List<Order> orders = orderDao.getAllCriteriaOrders(user_id, isAdmin);
        for (Order order : orders) {
            getProductInfo(order.getOrderItemList(), isAdmin);
        }
        return orders;
    }

    public Order getOrderById(int user_id, int order_id, boolean isAdmin) throws EmptyResultDataAccessException {
        Order order = orderDao.getOrderById(user_id, order_id, isAdmin);
        getProductInfo(order.getOrderItemList(), isAdmin);
        return order;
    }

    private void getProductInfo(List<OrderItem> orderItemList, boolean isAdmin) {
        for (OrderItem orderItem : orderItemList) {
            orderItem.setProduct(productDao.getProductById(orderItem.getProduct_id(), isAdmin));
        }
    }

    public void updateOrderStatus(int user_id, int order_id, boolean isAdmin, int status) throws EmptyResultDataAccessException, DataIntegrityViolationException, InvalidOrderStatusUpdateException {
        orderDao.updateOrderStatus(user_id, order_id, isAdmin, status);
    }

    public int addNewOrder(Order order) {
        return orderDao.addOrder(order);
    }

    public void addNewOrderItem(List<OrderItem> orderItemList) {
        for (OrderItem orderItem : orderItemList) {
            orderItemDao.addOrderItem(orderItem);
        }
    }
}
