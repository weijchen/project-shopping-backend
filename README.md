# project-shopping-backend

## Getting started
- Install dependencies (listed under `pom.xml`)
- Create database and tables with sql scripts (CreateTableAuth.sql, CreateTableContent.sql)
- Connect to database
  - replace `database.hibernate.username` and `database.hibernate.password`
- Ready to go!
